package assignment;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
/*    
	public void service(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		response.getWriter().println(firstName + " " + lastName);
	}
*/
	/**
	 * @see Servlet#init(ServletConfig)
	 */
    @Override
	public void init(ServletConfig config) throws ServletException
    {
    	System.out.println("Seth's Init");
	}

	/**
	 * @see Servlet#destroy()
	 */
    @Override
	public void destroy()
    {
    	System.out.println("Seth's Destroy");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		//response.getWriter().println(firstName + " " + lastName);
		request.setAttribute("firstName", firstName);
		request.setAttribute("lastName", lastName);
	    request.getRequestDispatcher("/TestResponse.jsp").forward(request, response);
		System.out.println("Seth's doGet");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		//response.getWriter().println(firstName + " " + lastName);
		
		if ((!firstName.equals(null) && !firstName.isEmpty()) && (!lastName.equals(null) && !lastName.isEmpty()))
		{
			request.setAttribute("firstName", firstName);
			request.setAttribute("lastName", lastName);
		    request.getRequestDispatcher("/TestResponse.jsp").forward(request, response);
			System.out.println("Seth's doPost");
		}
		else
		{
			String errorMessage = "Incomplete information. Missing:<br>";
			if (firstName.equals(null) || firstName.isEmpty())
			{
				errorMessage += "First Name<br>";
			}
			if (lastName.equals(null) || lastName.isEmpty())
			{
				errorMessage += "Last Name<br>";
			}
			request.setAttribute("errorMessage", errorMessage);
			request.getRequestDispatcher("/TestError.jsp").forward(request, response);
		}
	}
}
