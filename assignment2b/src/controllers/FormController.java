package controllers;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.User;

@ManagedBean
@ViewScoped
public class FormController 
{

	public String onSubmit(User user)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("user", user);
		return "TestResponse.xhtml?faces-redirect=true";
	}
	
	public String onFlash(User user) throws IOException
	{
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getFlash().put("user", user);
		return "TestResponse2.xhtml?faces-redirect=true";
	}
}
