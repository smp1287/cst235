package models;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;

@ManagedBean(name = "userDB")
@SessionScoped
public class UserDatabase implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Map<String, UserRegistration> userDB = new HashMap<>();
	String s = "";
    
	public UserDatabase() {
    	userDB.put("admin", new UserRegistration("Developer", "Login", "admin", "admin", 
    			   "admin@UserRegistration.com", "123 GCU BLVD AZ 86442", "928-303-2088"));
    }
    
	public String dbRegister(String userName, UserRegistration userData) {
		if (userDB.containsKey(userName)) {
			FacesMessage msg = 
	                new FacesMessage("User Name Already Exists.", 
	                        "User Name already Exists.");
	            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	            throw new ValidatorException(msg);
		}
		else 
		{
			userDB.put(userName, userData);
			return "success?faces-redirect=true";
		}
	}
	
	public UserRegistration dbLookup(String userName) {
		return userDB.get(userName);
	}
	
	public Map<String, UserRegistration> getUserDB() {
		return userDB;
	}
	
	@Override
	public String toString() {
		for(Map.Entry<String, UserRegistration> entry: userDB.entrySet()) {
			s += entry.toString() + "\n";
		}
		return s;
	}
	
/*
Testing User Database Methods
	public static void main(String[] args) {
		System.out.println(new UserDatabase().toString());
		UserDatabase db = new UserDatabase();
		UserRegistration user = new UserRegistration();
		user = db.dbLookup("admin");
		System.out.println(user.toString());		
	}
*/
}