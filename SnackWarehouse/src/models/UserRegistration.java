package models;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "register")
@SessionScoped
public class UserRegistration {

	private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String email;
    private String address;
    private String contact;

    public UserRegistration() {
    	//No Arg Constructor
    }
    
    public UserRegistration(String firstName, String lastName, String userName, String password, String email,
			String address, String contact) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.address = address;
		this.contact = contact;
	}
   
    //Object-Oriented constructor used when adding user to Hashmap in UserDatabase.java
    public UserRegistration (UserRegistration user) {
    	this.firstName = user.getFirstName();
    	this.lastName = user.getLastName();
    	this.userName = user.getUserName();
    	this.password = user.getPassword();
    	this.email = user.getEmail();
    	this.address = user.getAddress();
    	this.contact = user.getContact();
    }
    
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String name) {
        this.firstName = name;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String name) {
        this.lastName = name;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String name) {
        this.userName = name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }

	@Override
	public String toString() {
		return "UserRegistration [firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName
				+ ", password=" + password + ", email=" + email + ", address=" + address + ", contact=" + contact + "]";
	}
} 
